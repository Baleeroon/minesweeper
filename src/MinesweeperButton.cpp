#include "MinesweeperButton.h"

#include <QMouseEvent>

MinesweeperButton::MinesweeperButton(int posX, int posY, QWidget *parent) :
    QPushButton(parent),
    _posX(posX),
    _posY(posY),
    _enabled(true),
    _flag(false),
    _question(false)
{
    setFixedSize(30, 30);
    connect(this, SIGNAL(clicked()), this, SLOT(onClicked()));
}

MinesweeperButton::~MinesweeperButton()
{}

void MinesweeperButton::onClicked()
{
    emit leftClickedField(_posX, _posY);
}

void MinesweeperButton::mousePressEvent(QMouseEvent *e)
{
    if (e->button() == Qt::LeftButton)
    {
        if (_enabled && !_flag && !_question)
            click();
    }
    else if (e->button() == Qt::RightButton)
    {
        if (_enabled)
            emit rightClickedField(_posX, _posY);
    }
}
