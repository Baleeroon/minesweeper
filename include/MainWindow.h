#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include "MinesweeperButton.h"

typedef QVector<MinesweeperButton*> Buttons1D;
typedef QVector<Buttons1D> Buttons2D;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected:
    void placeMines(int posX, int posY, int mines);
    void incrementNeighbours(int x, int posY);
    void drawFields(int x, int y, int mines);
    void clearFields();
    void resizeWindow();
    void uncheckAllOptions();
    void showNeigbours(int posX, int posY);
    void disableAllFields();
    void showRestMines(int posX, int posY);
    void flagRestMines();

private slots:
    void onLeftClicked(int x, int y);
    void onRightClicked(int x, int y);
    void onSmileClicked();
    void addTime();
    void startBeginner();
    void startAdvance();
    void startExpert();
    void stopTimer();

private:
    Ui::MainWindow *ui;

    Buttons2D buttons;

    QTimer _timer;

    int _width;
    int _height;
    int _mines;
    int _minesLeft;
    const int _smileFieldSize;
    const int _margin;
    int _elapsedTime;
    int _allFields;

    bool _firstClick;
};

#endif // MAINWINDOW_H
