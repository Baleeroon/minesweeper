#ifndef FIELD_H
#define FIELD_H

class Field
{
public:
    Field() : _mine(false), _value(0)
    {}

    void setMine(bool mine) { _mine = mine; }
    bool isMine() { return _mine; }

    void setValue(int value) { _value = value; }
    int getValue() { return _value; }
private:
    bool _mine;
    int _value;
};

#endif // FIELD_H
