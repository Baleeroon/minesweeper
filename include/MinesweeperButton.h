#ifndef MINESWEEPERBUTTON_H
#define MINESWEEPERBUTTON_H

#include <QPushButton>
#include "Field.h"

class MinesweeperButton : public QPushButton, public Field
{
    Q_OBJECT
public:
    MinesweeperButton(int posX, int posY, QWidget *parent);
    ~MinesweeperButton();

    bool isEnabled() { return _enabled; }

    void setFlag(bool flag = true) { _flag = flag; }
    bool isFlag() { return _flag; }

    void setQuestionMark(bool mark = true) { _question = mark; }
    bool isQuestionMark() { return _question; }

public slots:
    void setEnabled(bool enabled) { _enabled = enabled; }

signals:
    void leftClickedField(int, int);
    void rightClickedField(int, int);

private slots:
    void onClicked();
    void mousePressEvent(QMouseEvent *e);

private:

    const int _posX;
    const int _posY;
    bool _enabled;
    bool _flag;
    bool _question;
};

#endif // MINESWEEPERBUTTON_H
