#include "MainWindow.h"
#include "ui_MainWindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    _smileFieldSize(75),
    _margin(10),
    _elapsedTime(0),
    _allFields(0),
    _firstClick(true)
{
    _timer.setInterval(1000);
    connect(&_timer, SIGNAL(timeout()), this, SLOT(addTime()));

    ui->setupUi(this);

    drawFields(8, 8, 10);
    resizeWindow();

    connect(ui->smileButton, SIGNAL(clicked()), this, SLOT(onSmileClicked()));
    connect(ui->actionNew_Game, SIGNAL(triggered()), this, SLOT(onSmileClicked()));
    connect(ui->actionPause, SIGNAL(triggered()), this, SLOT(stopTimer()));
    connect(ui->actionBeginner, SIGNAL(triggered()), this, SLOT(startBeginner()));
    connect(ui->actionAdvanced, SIGNAL(triggered()), this, SLOT(startAdvance()));
    connect(ui->actionExpert, SIGNAL(triggered()), this, SLOT(startExpert()));
    connect(ui->actionExit, SIGNAL(triggered()), this, SLOT(close()));

    ui->smileButton->setIconSize(QSize(50, 50));

    QIcon minesweeper(":/minesweeper/icons/mineRaw.svg");
    setWindowIcon(minesweeper);
}

MainWindow::~MainWindow()
{
    clearFields();
    delete ui;
}

void MainWindow::placeMines(int posX, int posY, int mines)
{
    QVector<QPair<int, int>> minesPos;
    minesPos.push_back(QPair<int, int>(posX, posY));
    int createdMines = 0;
    srand(time(nullptr));
    while(createdMines != mines)
    {
        int xNum = rand() % _width;
        int yNum = rand() % _height;

        QPair<int, int> pos(xNum, yNum);

        if (!minesPos.contains(pos))
        {
            minesPos.push_back(pos);

            buttons[xNum][yNum]->setMine(true);
            createdMines++;
            incrementNeighbours(xNum, yNum);
        }
    }
}

void MainWindow::incrementNeighbours(int posX, int posY)
{
    for (int x = posX - 1; x < posX + 2; x++)
    {
        if (x >= 0 && x < _width)
        {
            for (int y = posY - 1; y < posY + 2; y++)
            {
                if (y >= 0 && y < _height && !buttons[x][y]->isMine())
                    buttons[x][y]->setValue(buttons[x][y]->getValue() + 1);
            }
        }
    }
}

void MainWindow::drawFields(int x, int y, int mines)
{
    _width = x;
    _height = y;

    QIcon buttonSmile(":/minesweeper/icons/buttonSmile.svg");
    ui->smileButton->setIcon(buttonSmile);

    QIcon emptyField(":/minesweeper/icons/emptyField.svg");

    _allFields = 0;
    for (int i = 0; i < _width; i++)
    {
        buttons.push_back(Buttons1D());
        for (int j = 0; j < _height; j++)
        {
            buttons[i].push_back(new MinesweeperButton(i, j, ui->centralwidget));

            buttons[i][j]->move(i * 30 + _margin, j * 30 + _smileFieldSize);
            buttons[i][j]->show();
            buttons[i][j]->setIcon(emptyField);
            buttons[i][j]->setIconSize(QSize(30, 30));
            connect(buttons[i][j], SIGNAL(leftClickedField(int, int)), this, SLOT(onLeftClicked(int, int)));
            connect(buttons[i][j], SIGNAL(rightClickedField(int, int)), this, SLOT(onRightClicked(int, int)));
            _allFields++;
        }
    }

    _elapsedTime = 0;
    ui->timeLabel->setNum(_elapsedTime);
    _timer.stop();

    _mines = mines;
    _minesLeft = _mines;
    ui->minesNumber->setText(QString::number(_mines));
}

void MainWindow::clearFields()
{
    for (int x = 0; x < _width; x++)
    {
        for (int y = 0; y < _height; y++)
        {
            buttons[x][y]->hide();
            buttons[x][y]->close();
            buttons[x][y]->deleteLater();
        }
    }
    buttons.clear();
}

void MainWindow::resizeWindow()
{
    int width = _width * 30 + 2 * _margin;
    int height = _height * 30 + 4 *_margin + _smileFieldSize;

    setMaximumSize(width, height);
    setMinimumSize(width, height);
    resize(width, height);

    this->statusBar()->setSizeGripEnabled(false);
}

void MainWindow::uncheckAllOptions()
{
    ui->actionBeginner->setChecked(false);
    ui->actionAdvanced->setChecked(false);
    ui->actionExpert->setChecked(false);
}

void MainWindow::showNeigbours(int posX, int posY)
{
    for (int x = posX - 1; x < posX + 2; x++)
    {
        if (x >= 0 && x < _width)
        {
            for (int y = posY - 1; y < posY + 2; y++)
            {
                if (y >= 0 && y < _height && !buttons[x][y]->isMine())
                {
                    if (buttons[x][y]->isEnabled())
                        buttons[x][y]->click();
                }
            }
        }
    }
}

void MainWindow::disableAllFields()
{
    for (int i = 0; i < _width; i++)
    {
        for (int j = 0; j < _height; j++)
        {
            buttons[i][j]->setEnabled(false);
        }
    }
}

void MainWindow::showRestMines(int posX, int posY)
{
    QIcon mineGood(":/minesweeper/icons/mineGood.svg");
    QIcon mineBad(":/minesweeper/icons/mineBad.svg");
    QIcon mineCrossed(":/minesweeper/icons/mineCrossed.svg");

    for (int i = 0; i < _width; i++)
    {
        for (int j = 0; j < _height; j++)
        {
            if (buttons[i][j]->isFlag())
            {
                if (!buttons[i][j]->isMine())
                    buttons[i][j]->setIcon(mineCrossed);
            }
            else if (buttons[i][j]->isMine())
            {
                if (posX == i && posY == j)
                    buttons[i][j]->setIcon(mineBad);
                else
                    buttons[i][j]->setIcon(mineGood);
            }
        }
    }
}

void MainWindow::flagRestMines()
{
    QIcon flag(":/minesweeper/icons/flag.svg");

    for (int i = 0; i < _width; i++)
    {
        for (int j = 0; j < _height; j++)
        {
            if (buttons[i][j]->isMine())
                buttons[i][j]->setIcon(flag);
        }
    }
}

void MainWindow::onLeftClicked(int x, int y)
{
    buttons[x][y]->setEnabled(false);
    if (!_timer.isActive())
        _timer.start();

    QIcon _0_(":/minesweeper/icons/0.svg");
    QIcon _1_(":/minesweeper/icons/1.svg");
    QIcon _2_(":/minesweeper/icons/2.svg");
    QIcon _3_(":/minesweeper/icons/3.svg");
    QIcon _4_(":/minesweeper/icons/4.svg");
    QIcon _5_(":/minesweeper/icons/5.svg");
    QIcon _6_(":/minesweeper/icons/6.svg");
    QIcon _7_(":/minesweeper/icons/7.svg");
    QIcon _8_(":/minesweeper/icons/8.svg");
    QIcon buttonDeath(":/minesweeper/icons/buttonDeath.svg");
    QIcon buttonGlass(":/minesweeper/icons/buttonGlass.svg");

    if (_firstClick)
    {
        placeMines(x, y, _mines);
        _firstClick = false;
    }

    if (buttons[x][y] && buttons[x][y]->isMine())
    {
        _timer.stop();
        ui->smileButton->setIcon(buttonDeath);
        disableAllFields();
        showRestMines(x, y);
    }
    else if (buttons[x][y]->getValue() == 0)
    {
        buttons[x][y]->setIcon(_0_);
        showNeigbours(x, y);
    }
    else
    {
        switch (buttons[x][y]->getValue())
        {
            case 1:
                buttons[x][y]->setIcon(_1_);
                break;
            case 2:
                buttons[x][y]->setIcon(_2_);
                break;
            case 3:
                buttons[x][y]->setIcon(_3_);
                break;
            case 4:
                buttons[x][y]->setIcon(_4_);
                break;
            case 5:
                buttons[x][y]->setIcon(_5_);
                break;
            case 6:
                buttons[x][y]->setIcon(_6_);
                break;
            case 7:
                buttons[x][y]->setIcon(_7_);
                break;
            case 8:
                buttons[x][y]->setIcon(_8_);
                break;
        }
    }
    _allFields--;

    if (_allFields == _mines)
    {
        ui->smileButton->setIcon(buttonGlass);
        _timer.stop();
        flagRestMines();
        ui->minesNumber->setNum(0);
        disableAllFields();
    }
}

void MainWindow::onRightClicked(int x, int y)
{
    QIcon flag(":/minesweeper/icons/flag.svg");
    QIcon emptyField(":/minesweeper/icons/emptyField.svg");
    QIcon questionMark(":/minesweeper/icons/questionMark.svg");

    if (!buttons[x][y]->isFlag() && !buttons[x][y]->isQuestionMark())
    {
        buttons[x][y]->setIcon(flag);
        buttons[x][y]->setFlag();
        _minesLeft--;
    }
    else if (buttons[x][y]->isFlag())
    {
        buttons[x][y]->setIcon(questionMark);
        buttons[x][y]->setQuestionMark();
        buttons[x][y]->setFlag(false);
        _minesLeft++;
    }
    else if (buttons[x][y]->isQuestionMark())
    {
        buttons[x][y]->setIcon(emptyField);
        buttons[x][y]->setQuestionMark(false);
    }
    ui->minesNumber->setNum(_minesLeft);
}

void MainWindow::onSmileClicked()
{
    clearFields();
    drawFields(_width, _height, _mines);
    _firstClick = true;
}

void MainWindow::addTime()
{
    _elapsedTime++;
    ui->timeLabel->setNum(_elapsedTime);
}

void MainWindow::startBeginner()
{
    uncheckAllOptions();
    ui->actionBeginner->setChecked(true);

    clearFields();
    drawFields(8, 8, 10);
    _firstClick = true;
    resizeWindow();
}

void MainWindow::startAdvance()
{
    uncheckAllOptions();
    ui->actionAdvanced->setChecked(true);

    clearFields();
    drawFields(16, 16, 40);
    _firstClick = true;
    resizeWindow();
}

void MainWindow::startExpert()
{
    uncheckAllOptions();
    ui->actionExpert->setChecked(true);

    clearFields();
    drawFields(30, 16, 99);
    _firstClick = true;
    resizeWindow();
}

void MainWindow::stopTimer()
{
    _timer.stop();
}
