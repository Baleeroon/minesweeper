greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QT += core gui

TARGET = Minesweeper
MOC_DIR = $$PWD/temp
OBJECTS_DIR = $$PWD/temp
UI_DIR = $$PWD/temp
RCC_DIR = $$PWD/temp

DESTDIR = $$PWD/bin

TEMPLATE = app

CONFIG += cs++11
CONFIG -= debug_and_release

INCLUDEPATH += $$PWD/include

SOURCES += \
    $$PWD/src/MainWindow.cpp \
    $$PWD/src/main.cpp \
    $$PWD/src/MinesweeperButton.cpp

HEADERS += \
    $$PWD/include/MainWindow.h \
    $$PWD/include/Field.h \
    $$PWD/include/MinesweeperButton.h

FORMS += \
    $$PWD/forms/MainWindow.ui

RESOURCES += \
    $$PWD/res/icons.qrc
